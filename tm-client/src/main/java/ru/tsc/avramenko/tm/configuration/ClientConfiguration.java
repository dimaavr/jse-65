package ru.tsc.avramenko.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.tsc.avramenko.tm.endpoint.*;

@ComponentScan("ru.tsc.avramenko.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public AdminDataEndpoint adminDataEndpoint() {
        @NotNull final AdminDataEndpointService adminDataEndpointService =
                new AdminDataEndpointService();
        return adminDataEndpointService.getAdminDataEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        @NotNull final ProjectEndpointService projectEndpointService =
                new ProjectEndpointService();
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
    ) {
        @NotNull final SessionEndpointService sessionEndpointService =
                new SessionEndpointService();
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        @NotNull final TaskEndpointService taskEndpointService =
                new TaskEndpointService();
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        @NotNull final UserEndpointService userEndpointService =
                new UserEndpointService();
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    public AdminUserEndpoint adminUserEndpoint() {
        @NotNull final AdminUserEndpointService adminUserEndpointService =
                new AdminUserEndpointService();
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

}