package ru.tsc.avramenko.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${backup.interval}")
    private Integer backupInterval;

    @NotNull
    @Value("${password.secret}")
    private String passwordSecret;

    @NotNull
    @Value("${password.iteration}")
    private Integer passwordIteration;

    @NotNull
    @Value("${session.secret}")
    private Integer signatureSecret;

    @NotNull
    @Value("${session.iteration}")
    private Integer signatureIteration;

    @NotNull
    @Value("${server.port}")
    private Integer serverPort;

    @NotNull
    @Value("${server.host}")
    private String serverHost;

    @NotNull
    @Value("${jdbc.user}")
    private String jdbcUser;

    @NotNull
    @Value("${jdbc.password}")
    private String jdbcPass;

    @NotNull
    @Value("${jdbc.url}")
    private String jdbcUrl;

    @NotNull
    @Value("${jdbc.driver}")
    private String jdbcDriver;

    @NotNull
    @Value("${hibernate.dialect}")
    private String hibernateDialect;

    @NotNull
    @Value("${hibernate.hbm2ddl_auto}")
    private String hibernateHbm2ddl;

    @NotNull
    @Value("${hibernate.show_sql}")
    private String hibernateShowSql;

    @NotNull
    @Value("${hibernate.cache.use_second_level_cache}")
    private String hibernateCacheUseSecondLevelCache;

    @NotNull
    @Value("${hibernate.cache.use_query_cache}")
    private String hibernateCacheUseQueryCache;

    @NotNull
    @Value("${hibernate.cache.use_minimal_puts}")
    private String hibernateCacheUseMinimalPuts;

    @NotNull
    @Value("${hibernate.cache.hazelcast.use_lite_member}")
    private String hibernateCacheHazelcastUseLiteMember;

    @NotNull
    @Value("${hibernate.cache.region_prefix}")
    private String hibernateCacheRegionPrefix;

    @NotNull
    @Value("${hibernate.cache.provider_configuration_file_resource_path}")
    private String hibernateCacheProviderConfigurationFileResourcePath;

    @NotNull
    @Value("${hibernate.cache.region.factory_class}")
    private String hibernateCacheRegionFactoryClass;

}