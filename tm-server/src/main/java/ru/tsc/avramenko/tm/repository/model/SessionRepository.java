package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.avramenko.tm.model.Session;

import java.util.List;

public interface SessionRepository extends JpaRepository<Session, String> {

    @Query("SELECT e FROM Session e WHERE e.id = :id")
    @Nullable Session findSessionById(@Param("id") @NotNull String id);

    @Modifying
    @Query("DELETE FROM Session e")
    void clear();

    @Query("SELECT e FROM Session e WHERE e.user.id = :userId")
    @Nullable List<Session> findAllById(@Param("userId") @NotNull String userId);

    @Modifying
    @Query("DELETE FROM Session e WHERE e.user.id = :userId")
    void clear(@Param("userId") @NotNull String userId);

}