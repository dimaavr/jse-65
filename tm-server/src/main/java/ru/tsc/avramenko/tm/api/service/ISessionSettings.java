package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ISessionSettings {

    @NotNull
    Integer getSignatureSecret();

    @NotNull
    Integer getSignatureIteration();

}