package ru.tsc.avramenko.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRestEndpoint {

    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @PostMapping("/create")
    Task create(@RequestBody Task task);

    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody List<Task> tasks);

    @PostMapping("/save")
    Task save(@RequestBody Task task);

    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody List<Task> tasks);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}