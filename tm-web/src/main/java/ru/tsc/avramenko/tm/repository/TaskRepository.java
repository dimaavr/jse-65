package ru.tsc.avramenko.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Repository
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    @NotNull
    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        save(new Task("Task1", "Desc1"));
        save(new Task("Task2"));
        save(new Task("Task3", "Desc3"));
    }

    public Task create() {
        return save(new Task());
    }

    public Task save(@NotNull Task task) {
        tasks.put(task.getId(), task);
        return task;
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull String id) {
        return tasks.get(id);
    }

    public void removeById(@Nullable String id) {
        tasks.remove(id);
    }

    public void clear() {
        tasks.clear();
    }

}