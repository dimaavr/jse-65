package ru.tsc.avramenko.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRestEndpoint {

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @PostMapping("/create")
    Project create(@RequestBody Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody List<Project> projects);

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody List<Project> projects);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}