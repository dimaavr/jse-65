package ru.tsc.avramenko.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.IProjectRestEndpoint;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.repository.ProjectRepository;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectRepository repository;

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return (repository.findAll());
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody Project project) {
        return repository.save(project);
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody List<Project> projects) {
        projects.forEach(repository::save);
        return projects;
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        return repository.save(project);
    }

    @Override
    @PostMapping("/saveAll")
    public List<Project> saveAll(@RequestBody List<Project> projects) {
        projects.forEach(repository::save);
        return projects;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        repository.clear();
    }

}