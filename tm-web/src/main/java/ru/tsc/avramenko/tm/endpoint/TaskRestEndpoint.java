package ru.tsc.avramenko.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.api.ITaskRestEndpoint;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return (repository.findAll());
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody Task task) {
        return repository.save(task);
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody List<Task> tasks) {
        tasks.forEach(repository::save);
        return tasks;
    }

    @Override
    @PostMapping("/save")
    public Task save(@RequestBody Task task) {
        return repository.save(task);
    }

    @Override
    @PostMapping("/saveAll")
    public List<Task> saveAll(@RequestBody List<Task> tasks) {
        tasks.forEach(repository::save);
        return tasks;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        repository.clear();
    }

}